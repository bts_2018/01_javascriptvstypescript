## TypeScript ##

TypeScript is object oriented JavaScript. TypeScript supports object-oriented programming features like classes, interfaces, etc. 
A class in terms of OOP is a blueprint for creating objects. A class encapsulates data for the object. Typescript gives built in support for this concept called class.

	:::javascript
	export class Animal { 

		//attribute 
		name:string;
		age:number; 

		//constructor 
		constructor(name:string,age:number) { 
			this.name = name;
			this.age = age;
		}  
		
		//method 
		grow():number { 
			return this.age + 1;
		} 
	}  

	var x = new Animal("Bob",6);
    this.name  = x.name;
    this.age  = x.grow();

# GET START #
https://angular.io/

# ANGULAR CHANGE LOG
https://github.com/angular/angular/blob/master/CHANGELOG.md

# DIFFERENCE #
http://www.codekul.com/blog/difference-between-angular-2-4-5/